// [REQUIRE access]
const User = require("../models/User");
const Product = require("../models/Product");

const auth = require("../auth")

const bcrypt = require("bcrypt");


// [USER REGISTRATION]
/* [BUSINESS LOGIC]	User Registration + Email Existence
	1. Create a new User object using the User model and the information from the request body (postman).
	2. Check if there is a duplicate email by using mongoose "find" method
	3. Use "then" method to send a response back to Postman
	4. If no duplicate email and if user is not underage, proceed with registration and ensure that password is encrypted
	5. Save the new User to the database
*/
const today = new Date();
const maxBirthday = new Date(today.getFullYear() - 18, today.getMonth(), today.getDate());

module.exports.userRegistration = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((existingUser) => {
      if (existingUser) {
        return { success: false, message: 'Email already exists. Please try a different one.' };
      } else if (new Date(reqBody.birthday) > maxBirthday) {
        return { success: false, message: "We're sorry, you need to be at least 18 years old to register" };
      } else {
        const newUser = new User({
          firstName: reqBody.firstName,
          lastName: reqBody.lastName,
          mobileNo: reqBody.mobileNo,
          birthday: reqBody.birthday,
          email: reqBody.email,
          password: bcrypt.hashSync(reqBody.password, 10),
        });
        return newUser.save().then(() => {
          return { success: true, message: 'User registered!' };
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return { success: false, message: 'An error occurred during registration.' };
    });
};


// [USER AUTHENTICATION]
/* [BUSINESS LOGIC] 
	1. Check the database if the user email exists
	2. Compare the password provided in the login form VERSUS the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return a statement if not
*/

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return { error: "User does not exist" };
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return { error: "Incorrect password" };
      }
    }
  });
};


// [USER PROFILE]
/* [BUSINESS LOGIC] 
	1. Find the document in database using user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend/Postman
*/

module.exports.getProfile = async (data) => {
  try {
    const result = await User.findById(data.userId);
    result.password = "this is not the user's password";
    return result;
  } catch (error) {
    // Handle any errors that occur during the database query
    throw error;
  }
};


// [CREATE ORDER - USER only]
/* [BUSINESS LOGIC] 
[BUSINESS LOGIC]
	1. Verify that the user making the order is a regular user
	2. Retrieve the user's information, including their ID and orders placed, from the MongoDB Atlas Database
	3. Check that the requested products are valid and available for purchase
	4. Calculate the total cost of the order based on the products' prices and any applicable discounts
	7. Add the ordered products to a new order document in the database, including the user ID, product IDs, order total, and date/time
	8. Update the product inventory in the database to reflect the decrease in quantity for the ordered products
	9. Send a confirmation message to the user with the order details and any additional information or instructions
*/


module.exports.placeOrder = async (data, isAdmin) => {
  if (isAdmin === true) {
    return Promise.resolve("Admins cannot place an order");
  } else {
    try {
      const user = await User.findById(data.userId);
      if (!user) {
        return Promise.resolve("User not found");
      }
      user.orders.push({ productId: data.productId });
      await user.save();

      const product = await Product.findById(data.productId);
      if (!product) {
        return Promise.resolve("Product not found");
      }
      // Modify and save the product data as needed
      await product.save();

      return Promise.resolve("Order placed!");
    } catch (error) {
      console.error(error);
      return Promise.reject(error); // Reject the promise with the error
    }
  }
};






