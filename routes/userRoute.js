const express = require("express");
const router = express.Router();
const app = express();

const userController = require("../controllers/userController");

const auth = require("../auth");

// [ROUTE 1] User registration
router.post("/registration", (req, res) => {
	userController.userRegistration(req.body).then(resultFromController => res.send(resultFromController));
});


// [ROUTE 2] User authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// [ROUTE 3] Retrieve user profile
router.post("/profile", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// [ROUTE 4] 

router.post("/placeOrder", auth.verify, (req, res) => {
  let userId = auth.decode(req.headers.authorization).id;
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  let data = {
    userId: userId,
    productId: req.body.productId
  };
  userController.placeOrder(data, isAdmin).then(resultFromController => res.send(resultFromController));
});


module.exports = router;