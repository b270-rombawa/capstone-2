const mongoose = require('mongoose');

const today = new Date();
const maxBirthday = new Date(today.getFullYear() - 18, today.getMonth(), today.getDate());

// [SCHEMA] --

const userSchema = new mongoose.Schema({
	firstName: {
		type: String, 
		minlength: 2,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		minlength: 2,
		required: [true, "Last name is required"]
	},
	mobileNo: {
		type: String,
		minlength: 10,
		required: [true, "Mobile Number is required"]
	},
	birthday: {
		type: Date,
		max: maxBirthday,
		required: [true, "Birthday is required"]
	},
	email: {
		type: String,
		minlength: 5,
		required: [true, "Email is required"] 
	},
	password: {
		type: String,
		minlength: 8,
		required: [true, "Password is required"] 
	},
	isAdmin: {
		type: Boolean,
		default: false // regular user 
	},
	isActive: {
		type: Boolean,
		default: true
	},
	orders: [
		{
			product: {
				productName: {
					type: String,
				},
				quantity: {
					type: Number,
					default: 1
				}
			},
			totalAmount: {
				type: Number,
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);