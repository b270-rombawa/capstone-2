/*[ENTRY POINT]	where you will find the following:
	Server set-up
	Access to folders
	Application
	Middleware functions
	DB connection 
*/

// [DEPENDENCIES SET UP]
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// [ACCESS TO OTHER FOLDERS]
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");


// [APPLICATION]
const app = express();

// [MIDDLEWARE]
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use("/users", userRoute);
app.use("/products", productRoute);



// [DATABASE CONNECTION]
mongoose.connect("mongodb+srv://jenarombawalyn:admin123@zuitt-bootcamp.or0deu6.mongodb.net/capstone2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open", () => console.log(`Now connected to the MongoDB`));
mongoose.connection.on("disconnected", () => console.log(`Disconnected from MongoDB`));


app.listen(process.env.PORT || 5000, () => console.log(`Now connected to port ${process.env.PORT || 5000}`));