const express = require("express");
const router = express.Router();
const app = express();
const productController = require("../controllers/productController");

const auth = require("../auth");

// [ROUTE 1] create product for admin use only
router.post('/create', auth.verify, async (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  try {
    const resultFromController = await productController.createProduct(data);
    res.send(resultFromController);
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  }
});


// [ROUTE 2] retrieve ALL ACTIVE products
router.get("/allActive", (req, res) => {
	productController.activeProducts().then(resultFromController => res.send(resultFromController));
});

// [ROUTE 3] retrieve ALL ACTIVE/INACTIVE products
router.get("/allProducts", (req, res) => {
  productController.allProducts().then(resultFromController => res.send(resultFromController));
});

// [ROUTE 4] get a SINGLE product
router.get("/:name", (req, res) => {
  productController.getSingleProduct(req.params)
    .then(resultFromController => res.send(resultFromController));
});

// [ROUTE 5] update product, for admin use only
router.put("/:productId", auth.verify, (req, res) => {
  const productUpdate = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  productController.updateProduct(req.params.productId, productUpdate.product, productUpdate.isAdmin)
    .then(resultFromController => res.send(resultFromController));
});

// [ROUTE 6] archive product, for admin use only
router.patch("/:productId/archive", auth.verify, (req, res) => {
  const toArchiveProduct = {
    productId: req.params.productId,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  productController.archiveProduct(toArchiveProduct.productId, toArchiveProduct.isAdmin)
    .then(resultFromController => res.send(resultFromController));
});



module.exports = router;