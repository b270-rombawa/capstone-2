const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type : Number,
		required : [true, "Price is required"]
	},
	slots: {
		type: Number,
		required: [true, "Slots is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
	type : Date,
	default : new Date()
	},
	orders: [
	{
		orderId: {
			type: String
		},
		productName: {
			type: String
		},
		status: {
			type: String,
			default: "Order placed." 
		},
		orderedOn: {
			type: Date,
			default: new Date()
		}
	}]
});

module.exports = mongoose.model("Product", productSchema);