const Product = require("../models/Product");

// [PRODUCT CREATION for any users]
/* [BUSINESS LOGIC]	any user can create/add a product
	1. Create a new Product object using the User model and the information from the request body (postman).
	2. Save the new Product to the database if there's no error with connection
	3. test in postman
	4. refactor to make it accessible by admin only
*/
/*module.exports.createProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	});
	return newProduct.save().then((product, error) => {
		if (error) {
			return `Error in adding product`;
		} else {
			return `Product added successfully!`
		};
	});
};*/

// [PRODUCT CREATION - ADMIN only]
/* [BUSINESS LOGIC]
	1. Following the code above, copy and paste it here and refactor to make it accessible by admin only
	2. once user cannot access this function, admin can add product and save it to the database.
*/

module.exports.createProduct = (data) => {
  if (data.isAdmin) {
    const newProduct = new Product({
      name: data.product.name,
      description: data.product.description,
      price: data.product.price
    });

    return newProduct.save()
      .then(() => {
        return 'Product added successfully';
      })
      .catch((error) => {
        console.error(error);
        return 'Product creation failed';
      });
  }

  return Promise.resolve('User must be ADMIN to access this.');
};


// [RETRIEVE ALL ACTIVE PRODUCTS - all users]
/* [BUSINESS LOGIC]
  1. Query all ACTIVE Product collection in Postman/database to retrieve all products.
 	2. If there are products, return all the result to the client.
 	3. If there are no products, return an error message to the client.
*/

module.exports.activeProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

// [RETRIEVE ALL PRODUCTS - inactive & active, admin only]

module.exports.allProducts = () => {
	return Product.find().then((result) => {
		return result;
	});
};

// [RETRIEVE SINGLE PRODUCT - all users]
/* [BUSINESS LOGIC]
  1. Query a single product in Postman/database using the name field
 	2. Return the wanted/specific product result to the client.
 	3. If there are no products, return an error message to the client.
*/

module.exports.getSingleProduct = (reqParams) => {
  return Product.findOne({ name: reqParams.name })
    .then(result => {
      if (!result) {
        return "Product not available";
      } else {
        return result;
      };
    });
};

// [UPDATE PRODUCT INFO - ADMIN only]
/* [BUSINESS LOGIC]
  1. Authenticate if user is an admin using email, password, and token generation.
  2. Authorize admin to access product update API endpoint.
  3. Admin user must provide productId in reqparams to update a specific product.
  4. Admin should be able to modify product info in reqBody, such as name, description, and other relevant details.
  5. If update is successful, changes should be saved to the database and return a response indicating that changes have been saved.
*/

module.exports.updateProduct = (id, updatedProduct, isAdmin) => {
	if (!isAdmin) {
		return Promise.resolve(`User must be ADMIN to access this.`);
	}
	return Product.findByIdAndUpdate(id, updatedProduct, { new: true })
		.then(product => {
			if (product) {
				return `Product updated successfully`;
			} else {
				return `Product not found`;
			}
		})
		.catch(error => {
			return `Product update failed`;
		});
};

// [ARCHIVE A PRODUCT - ADMIN only]
/* [BUSINESS LOGIC]

	1. Authenticate if user is an admin using email, password, and token generation.
	2. Authorize admin to access product archive API endpoint.
	3. Admin user must provide productId in reqparams to archive a specific product.
	4. Check if the product exists in the database. If it doesn't, return an error message indicating that the product was not found.
	5. If the product exists, set its "isActive" property to false to indicate that it has been archived.
	6. Save the changes to the database and return a response indicating that the product has been archived successfully.
*/

// module.exports.archiveProduct = (id, isAdmin) => {
//   if (!isAdmin) {
//     return Promise.resolve(`User must be ADMIN to access this.`);
//   }

//   const updateActiveField = { isActive: false };

//   return Product.findByIdAndUpdate(id, updateActiveField, { new: true })
//     .then(product => {
//       if (product) {
//         return `Product archived successfully`;
//       } else {
//         return `Product not found`;
//       };
//     });
// };


module.exports.archiveProduct = (id, isAdmin) => {
  if (!isAdmin) {
    return Promise.resolve(`User must be ADMIN to access this.`);
  }

  return Product.findOneAndUpdate({ _id: id }, { isActive: false }, { new: true })
    .then(product => {
      if (product) {
        return `'${product.name}' has been successfully archived`;
      } else {
        return `Product not found`;
      }
    })
    .catch(error => {
      if (error.name === 'CastError') {
        return 'Product not found';
      } else {
        return `Product archive failed`;
      }
    });
};



module.exports.archiveProduct